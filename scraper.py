import requests
from bs4 import BeautifulSoup

START_PAGE = r'https://www.theguardian.com/world/middleeast'

def get_first_page():
    """"scrape the main pain page"""
    return requests.get(r'https://www.theguardian.com/world/middleeast')



def main():
    get_first_page()


if __name__ == "__main__":
    main()